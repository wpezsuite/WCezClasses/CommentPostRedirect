<?php
// As inspired by: https://theswedishbear.com/woocommerce-show-information-notice-after-review-submit/

namespace WPezSuite\WCezClasses\CommentPostRedirect;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'ClassAdminEmailTitles' ) ) {
	class ClassAdminEmailTitles implements InterfaceCommentPostRedirect {

		protected $_str_redirect_to_url;
		protected $_str_notice;
		protected $_str_notice_type;
		protected $_arr_notice_types;
		protected $_arr_post_types;

		public function __construct() {

			$this->setPropertyDefaults();
		}

		protected function setPropertyDefaults() {

			$this->_str_redirect_to_url = false;
			$this->_str_notice          = false;
			$this->_str_notice_type     = 'success';
			$this->_arr_notice_types    = [ 'error', 'notice', 'success' ];
			$this->_arr_post_types      = [ 'product' ];
		}

		/**
		 * you can pass in a URL string, a WP_Post object, or an int Post ID
		 *
		 * @param bool $mix
		 *
		 * @return bool
		 */
		public function setRedirectToURL( $mix = false ) {

			$str = $mix;
			if ( ! is_string( $str ) ) {
				https://developer.wordpress.org/reference/functions/get_permalink/
				$str = get_permalink( $mix );
			}

			if ( filter_var( $str, FILTER_VALIDATE_URL ) === true ) {
				$this->_str_redirect_to_url;

				return true;
			}

			return false;
		}

		public function setNotice( $str = false ) {

			if ( is_string( $str ) ) {

				$this->_str_notice = $str;

				return true;
			}

			return false;
		}

		public function setNoticeType( $str = false ) {

			if ( in_array( $str, $this->_arr_notice_types ) ) {

				$this->_str_notice_type = $str;

				return true;
			}

			return false;
		}

		public function filterCommentPostRedirect( $location, $comment ) {

			if ( is_string( $this->_str_redirect_to_url ) ) {
				$post_id       = $comment->comment_post_ID;
				$str_post_type = get_post_type( $post_id );
				if ( in_array( $str_post_type, $this->_arr_post_types ) ) {

					$location = $this->_str_redirect_to_url;
				}

				if ( $this->_str_notice !== false ) {

					wc_add_notice( $this->_str_notice, $this->_str_notice_type );
				}

			}

			return $location;
		}

	}
}
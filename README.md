## WCezClasses: WooCommerce Comment Post Redirect

__A clean and direct way to redirect after a product comment (aka review) is posted (aka submitted), and do it The ezWay.__

Optional: Add a notice (using wc_add_notice()) via the setNotice() method. 

As inspired by: https://theswedishbear.com/woocommerce-show-information-notice-after-review-submit/


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WCezClasses autoloader (link below)._

```
use WPezSuite\WCezClasses\CommentPostRedirect\ClassCommentPostRedirect as CPR;
use WPezSuite\WCezClasses\CommentPostRedirect\ClassHooks as Hooks;

$new_cpr = new CPR();
$new_cpr->setRedirectToURL({ URL string | WP_Post object | int Post ID);

$new_cpr->setNotice('Noticed to be displayed');

$new_hooks = new Hooks($new_cpr);
$new_hooks->register();
```



### HELPFUL LINKS
 
 - https://gitlab.com/WPezSuite/WCezClasses/WCezAutoload
 
 - https://theswedishbear.com/woocommerce-show-information-notice-after-review-submit/
 
  
### TODO

- Setter for redirected post types

- Redirect to URL based on product ID. 



### CHANGE LOG

- v0.0.2 - 19 May 2019
    - Added wc_add_notice() and supporting properties and methods

- v0.0.1 - 16 May 2019
    - Hey! Ho!! Let's go!!! 